import { NgModule } from '@angular/core';
import {
  MdButtonModule, MdCheckboxModule, MdToolbarModule, MdMenuModule, MdIconModule,
  MdTabsModule
} from '@angular/material';

@NgModule({
  imports: [MdButtonModule, MdCheckboxModule, MdToolbarModule, MdMenuModule, MdIconModule, MdTabsModule],
  exports: [MdButtonModule, MdCheckboxModule, MdToolbarModule, MdMenuModule, MdIconModule, MdTabsModule],
})
export class CustomMaterialModule { }
